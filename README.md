# Setup

Follow instructions in https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/appengine/flexible/cloudsql

# Running locally

1. Run the proxy to allow connecting to your instance from your machine.

    $ ./$HOME/cloud_sql_proxy -dir /tmp/cloudsql -instances=holly-cottage-257410:europe-west2:holly-cottage-db=tcp:3306 -credential_file=$HOME/rabbie-cloud/secrets/sql_credentials.json

2. Set environment variables 

    $ source secrets/environs

3. Run ``create_tables.py`` to ensure that the database is properly configured and to create the tables needed for the sample.

4. Run the app

    $ python main.py
    
# Deploying app

1. Update the connection string in ``app.yaml`` with your configuration values. These values are used when the application is deployed.


