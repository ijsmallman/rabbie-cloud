from flask import request
import logging
import os


class NoTokenError(Exception):
    pass


class UnauthorizedError(Exception):
    pass


def authorise():
    """
    Authorise request

    Raises
    ------
    NoTokenError
        If token is not found
    UnauthorizedError
        If request is not authorized
    """

    token = None
    if 'Authorization' in request.headers:
        token = request.headers['Authorization']
    
    if not token:
        logging.warning("No token supplied")
        raise NoTokenError("No token supplied in request header")
    
    try:
        target_token = os.environ['REST_TOKEN']
    except:
        logging.error("Token not set on server")
        raise NoTokenError("Token not set on server")

    if token != target_token:
        logging.warning("Invalid API token: {}".format(token))
        raise UnauthorizedError("Invalid API token: {}".format(token))

    logging.debug("{} authorised".format(request.remote_addr))
