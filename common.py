import logging
import flask
from datetime import datetime, timezone
from typing import Tuple

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


def serialise_timestamp(timestamp: datetime) -> str:
    """
    Serialise a timestamp into a string

    Parameters
    ----------
    timestamp: datetime

    Returns
    -------
    serialised: str
    """
    return timestamp.strftime(DATETIME_FORMAT)


def ts_to_ms(timestamp: datetime) -> float:
    return 1000 * timestamp.replace(tzinfo=timezone.utc).timestamp()


def get_from_to() -> Tuple[datetime, datetime]:
    """
    Pull from and to stamp date times from request arguments.
    If not null, attempt to format them into datetime objects.

    Returns
    -------
    from_stamp: datetime
    to_stamp: datetime

    Raises
    ------
    ValueError
        If cannot parse args.
    """
    from_stamp = flask.request.args.get("from", None)
    to_stamp = flask.request.args.get("to", None)

    if from_stamp is not None:
        from_stamp = datetime.strptime(
            from_stamp,
            DATETIME_FORMAT
        )

    if to_stamp is not None:
        to_stamp = datetime.strptime(
            to_stamp,
            DATETIME_FORMAT
        )

    logging.debug("Parsed argument \"from\" to: {}".format(from_stamp))
    logging.debug("Parsed argument \"to\" to: {}".format(to_stamp))

    return from_stamp, to_stamp
