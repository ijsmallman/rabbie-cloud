import logging
import os
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
from http import HTTPStatus
from flask import Flask, jsonify, request

from authorisation import authorise, NoTokenError, UnauthorizedError
from common import serialise_timestamp, DATETIME_FORMAT, get_from_to

app = Flask(__name__)


app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class Reading(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    stamp = db.Column(db.DateTime)
    label = db.Column(db.String(16))
    reading = db.Column(db.Float)
    units = db.Column(db.String(16))

    def __init__(self, stamp: datetime, label: str, reading: float, units: str) -> None:
        self.stamp = stamp
        self.label = label
        self.reading = reading
        self.units = units

    def __repr__(self) -> str:
        return "<Reading(stamp='{}', label='{}', reading='{}', unts='{}')>".format(
            self.stamp,
            self.label,
            self.reading,
            self.units
        )

    @property
    def serialised(self):
        return {
            'stamp': serialise_timestamp(self.stamp),
            'label': self.label,
            'reading': self.reading,
            'units': self.units
        }


@app.route('/', methods=['GET', 'POST'])
def index():

    try:
        authorise()
    except (NoTokenError, UnauthorizedError) as e:
        return jsonify(message=str(e)), HTTPStatus.UNAUTHORIZED

    if request.method == 'POST':

        data = request.json

        if data is None:
            return jsonify(message="Unsupported request payload"), HTTPStatus.UNSUPPORTED_MEDIA_TYPE

        deserialised_data = {}
        for k in ['stamp', 'label', 'reading', 'units']:
            try:
                v = data[k]
            except KeyError:
                return jsonify(message="Payload missing {}".format(k)), HTTPStatus.UNPROCESSABLE_ENTITY

            try:
                if k == 'stamp':
                    deserialised_data[k] = datetime.strptime(v, DATETIME_FORMAT)
                elif k == 'reading':
                    deserialised_data[k] = float(v)
                else:
                    deserialised_data[k] = v
            except ValueError:
                return jsonify(message="Cannot parse value {}".format(v)), HTTPStatus.UNPROCESSABLE_ENTITY

        reading = Reading(
            stamp=deserialised_data['stamp'],
            label=deserialised_data['label'],
            reading=deserialised_data['reading'],
            units=deserialised_data['units']
        )

        db.session.add(reading)
        db.session.commit()
        logging.info("Added {} to database".format(reading))

        return jsonify(message="Success"), HTTPStatus.OK

    else:

        from_ts, to_ts = get_from_to()

        if from_ts is None and to_ts is None:
            query = db.session.query(Reading)
        elif from_ts is None:
            query = db.session.query(Reading).filter(Reading.stamp <= to_ts)
        elif to_ts is None:
            query = db.session.query(Reading).filter(Reading.stamp >= from_ts)
        else:
            query = db.session.query(Reading).filter(
                Reading.stamp.between(from_ts, to_ts)
            )

        readings = query.order_by(sqlalchemy.asc(Reading.timestamp)).all()

        return jsonify([r.serialised for r in readings]), HTTPStatus.OK


@app.errorhandler(HTTPStatus.INTERNAL_SERVER_ERROR)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), HTTPStatus.INTERNAL_SERVER_ERROR


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
